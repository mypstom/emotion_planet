$(document).ready(function(){
    var socket = io();
    //進入星球所需時間
    let loadingPlanetTime = 5000;
    let planetIndex = 1;

    $(".loading").hide();
    $(".home").hide();
    $(".controller").hide();

    $(".home").fadeIn(1000);

    socket.on('goPlanet', function(val){
        planetIndex = val;
        goPlanet();
    });

    socket.on('leavePlanet', function(){
        leavePlanet();
    });

    createRangeSlider();
    var homeSlider, controlSlider, controlSlider1, controlSlider2, controlSlider3, controlSlider4;
    function createRangeSlider(){
        //控制群組
        controlSlider1 = document.getElementById('control-slider1');
        noUiSlider.create(controlSlider1, {
            start: [50],
            animate: false,
            orientation: "vertical",
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider1.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue1', values[0]);
        });

        controlSlider1.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer1', values[0]);
        });

        controlSlider2 = document.getElementById('control-slider2');
        noUiSlider.create(controlSlider2, {
            start: [50],
            animate: false,
            orientation: "vertical",
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider2.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue2', values[0]);
        });

        controlSlider2.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer2', values[0]);
        });

        controlSlider3 = document.getElementById('control-slider3');
        noUiSlider.create(controlSlider3, {
            start: [50],
            orientation: "vertical",
            animate: false,
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider3.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue3', values[0]);
        });

        controlSlider3.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer3', values[0]);
        });

        controlSlider4 = document.getElementById('control-slider4');
        noUiSlider.create(controlSlider4, {
            start: [50],
            animate: false,
            orientation: "vertical",
            range: {
                'min': [0],
                'max': [100]
            }
        });

        controlSlider4.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue4', values[0]);
        });

        controlSlider4.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer4', values[0]);
        });

        
        socket.on('changeValue1', function(val) {
            controlSlider1.noUiSlider.set(val)
        });
        socket.on('changeValue2', function(val) {
            controlSlider2.noUiSlider.set(val)
        });
        socket.on('changeValue3', function(val) {
            controlSlider3.noUiSlider.set(val)
        });
        socket.on('changeValue4', function(val) {
            controlSlider4.noUiSlider.set(val)
        });
        
    }

    function updateIcons(){
        for(let i=1; i<4; i++){
            let tempPath = './img/controller/icon/'+ planetIndex +'/'+ i +'.png';
            $("#icon"+(i+1)).attr("src", tempPath);
        }
    }

    function goPlanet(){
        updateIcons();
        $(".home").fadeOut(1000, function(){
            $(".controller").fadeIn(800);
        });
    }

    function leavePlanet(){
        $(".controller").fadeOut(1000, function(){
            $(".home").fadeIn(600);
        });
        controlSlider1.noUiSlider.reset();
        controlSlider2.noUiSlider.reset();
        controlSlider3.noUiSlider.reset();
        controlSlider4.noUiSlider.reset();
    }

});