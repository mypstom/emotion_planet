$(document).ready(function(){
    var socket = io();
    socket.emit('leavePlanet');
    //進入星球所需時間
    let loadingPlanetTime = 10000;


    $(".loading").hide();
    $(".select").hide();
    $(".home").hide();

    $(".home").fadeIn(1000, function(){
        $(this).on("click", function(){
            socket.emit('start');
            $(this).hide();
            $(".select").fadeIn(1200);
            //$(".controller").fadeIn(800);
        });
    });
    

    let planetIndex = 1;
    let pathPrefix = "img/captain/p";

    $(".select .right").on("click", function(){
        planetIndex++;
        if(planetIndex==6)planetIndex=1;
        socket.emit('moveRight');
        changePlanet();
    });

    $(".select .left").on("click", function(){
        planetIndex--;
        if(planetIndex==0)planetIndex=5;
        socket.emit('moveLeft');
        changePlanet();
    });
    
    createRangeSlider();
    var homeSlider, controlSlider, controlSlider1, controlSlider2, controlSlider3, controlSlider4;
    function createRangeSlider(){
        homeSlider = document.getElementById('home-slider');
        noUiSlider.create(homeSlider, {
            start: [50],
            orientation: "vertical",
            range: {
                'min': [0],
                'max': [100]
            }
        });
        homeSlider.noUiSlider.on('update', function (values, handle) {
            if(values[0]<=1){
                homeSlider.noUiSlider.set(50);
                socket.emit('goPlanet');
                goPlanet();
            }
        });

        controlSlider = document.getElementById('control-slider');
        noUiSlider.create(controlSlider, {
            start: [50],
            orientation: "vertical",
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider.noUiSlider.on('update', function (values, handle) {
            if(values[0]>=99){
                controlSlider.noUiSlider.set(50);
                leavePlanet();
            }
        });


        //控制群組
        controlSlider1 = document.getElementById('control-slider1');
        noUiSlider.create(controlSlider1, {
            start: [50],
            orientation: "vertical",
            animate: false,
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider1.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue1', values[0]);
        });
        controlSlider1.noUiSlider.on('update', function (values, handle) {
            console.log(values[0]);
            socket.emit('changeValueComputer1', values[0]);
        });

        controlSlider2 = document.getElementById('control-slider2');
        noUiSlider.create(controlSlider2, {
            start: [50],
            orientation: "vertical",
            animate: false,
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider2.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue2', values[0]);
        });
        controlSlider2.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer2', values[0]);
        });

        controlSlider3 = document.getElementById('control-slider3');
        noUiSlider.create(controlSlider3, {
            start: [50],
            orientation: "vertical",
            animate: false,
            range: {
                'min': [0],
                'max': [100]
            }
        });
        controlSlider3.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue3', values[0]);
        });
        controlSlider3.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer3', values[0]);
        });

        controlSlider4 = document.getElementById('control-slider4');
        noUiSlider.create(controlSlider4, {
            start: [50],
            animate: false,
            orientation: "vertical",
            range: {
                'min': [0],
                'max': [100]
            }
        });

        controlSlider4.noUiSlider.on('change', function (values, handle) {
            socket.emit('changeValue4', values[0]);
        });
        controlSlider4.noUiSlider.on('update', function (values, handle) {
            socket.emit('changeValueComputer4', values[0]);
        });
        
        socket.on('changeValue1', function(val) {
            controlSlider1.noUiSlider.set(val)
        });
        socket.on('changeValue2', function(val) {
            controlSlider2.noUiSlider.set(val)
        });
        socket.on('changeValue3', function(val) {
            controlSlider3.noUiSlider.set(val)
        });
        socket.on('changeValue4', function(val) {
            controlSlider4.noUiSlider.set(val)
        });
        
    }


    function updateIcons(){
        for(let i=1; i<4; i++){
            let tempPath = './img/controller/icon/'+ planetIndex +'/'+ i +'.png';
            $("#icon"+(i+1)).attr("src", tempPath);
        }
    }

    function goPlanet(){
        $(".select").fadeOut(1000, function(){
            $(".loading").fadeIn(600).delay(loadingPlanetTime).fadeOut(500, function(){
                console.log("進入星球：" + planetIndex);
                updateIcons();
                $(".controller").fadeIn(800);
            });
        });
    }

    function leavePlanet(){
        socket.emit('leavePlanet');
        $(".controller").fadeOut(1000, function(){
            $(".home").fadeIn(600);
        });
        controlSlider1.noUiSlider.reset();
        controlSlider2.noUiSlider.reset();
        controlSlider3.noUiSlider.reset();
        controlSlider4.noUiSlider.reset();
    }


    function changePlanet(){
        let tempPath = pathPrefix + planetIndex + ".png";
        console.log(planetIndex);
        $(".select").fadeOut(600, function(){
            $(this).css("background-image", 'url("' + tempPath) + '")';
            $(this).fadeIn(1000);
        });
    }


});

