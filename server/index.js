var path = require('path');
var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use('/', express.static(path.join(__dirname + '/public')));

app.get("/captain", function(req, res) {
    res.sendFile(__dirname + '/public/captain.html');
});

app.get("/crew", function(req, res) {
    res.sendFile(__dirname + '/public/crew.html');
});

io.on('connection', function(socket) {
    console.log("有使用者連線");
    let _socketId = socket.id;
    
    socket.on('changeValue1', function(val) {
        socket.broadcast.emit('changeValue1', val);
        
    });

    socket.on('changeValue2', function(val) {
        socket.broadcast.emit('changeValue2', val);
        
    });
    socket.on('changeValue3', function(val) {
        socket.broadcast.emit('changeValue3', val);
        
    });
    socket.on('changeValue4', function(val) {
        socket.broadcast.emit('changeValue4', val);
        
    });

    socket.on('changeValueComputer1', function(val) {
        io.emit('changeValueComputer1', {data: val});
    });
    socket.on('changeValueComputer2', function(val) {
        io.emit('changeValueComputer2', {data: val});
    });
    socket.on('changeValueComputer3', function(val) {
        io.emit('changeValueComputer3', {data: val});
    });
    socket.on('changeValueComputer4', function(val) {
        io.emit('changeValueComputer4', {data: val});
    });
    

    socket.on('goPlanet', function() {
        io.emit('goPlanet');
    });

    socket.on('leavePlanet', function() {
        io.emit('leavePlanet');
        console.log("Leave Planet");
    });

    socket.on('start', function() {
        io.emit('start');
        console.log("Start");
    });

    socket.on('moveRight', function() {
        io.emit('moveRight');
        console.log("move right");
    });

    socket.on('moveLeft', function() {
        io.emit('moveLeft');
        console.log("move left");
    });
    
});

http.listen(3000, function() {
    console.log("情緒星球伺服器啟動 at 3000 port");
});